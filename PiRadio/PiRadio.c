#include "/usr/include/math.h"
//#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <assert.h>
#include <malloc.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>

#define F_XTAL       (19229581.050215044276577479844352)             // calibrated 19.2MHz XTAL frequency 
#define F_PLLD_CLK   (26.0 * F_XTAL)                                 // 500MHz PLLD reference clock 

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

int  mem_fd;
char *gpio_mem, *gpio_map;
char *spi0_mem, *spi0_map;


// I/O access
volatile unsigned *gpio;
volatile unsigned *allof7e;

#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

// these definitions are for the bus address -> they need to be mapped into physical addresses.
#define CM_GP0CTL (0x7e101070)   // General Puropose clocks control address. 
#define CM_GP0DIV (0x7e101074)    // The divider and the DIVF values are stored in here
#define CLKBASE (0x7E101000)	 // the CLK base 
#define GPFSEL0 (0x7E200000)     // Select the General Purpose frequency. 
#define DMABASE (0x7E007000)	 // DMA base??? What is this witch craft?
#define PWMBASE  (0x7e20C000) 		/* PWM controller */


// ACCESS maps a bus address to a physical address. base is the 0x7Ennnnnn that we were talking about. 
#define ACCESS(base) *(volatile int*)((int)allof7e+base-0x7e000000)

#define SETBIT(base, bit) ACCESS(base) |= 1<<bit
#define CLRBIT(base, bit) ACCESS(base) &= ~(1<<bit)

struct GPCTL {
						// sets the number of bits that can be used to set the Clock Manager General Purpose Clocks Control (CM_GP0CTL) 
    char SRC         : 4;   		// the source of the information
    char ENAB        : 1;			// enable: YES
    char KILL        : 1; 			// KILL?? wtf?
    char             : 1;
    char BUSY        : 1;			// the busy bit
    char FLIP        : 1;			// inverts the clock generator input
    char MASH        : 2;			// MASH type
    unsigned int     : 13;
    char PASSWD      : 8;            // password? not sure why it's set to 8? 
};



void setup_fm()
{

    /* open /dev/mem  <- mem is the physical memory that we will write to to set bits and all that.*/
    if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
        printf("can't open /dev/mem \n");
        exit (-1);
    }
    
    // all of 7e is basically all the current bits that we have atm in the peripherals 
    // 
    allof7e = (unsigned *)mmap(
                  NULL,
                  0x01000000,  //len
                  PROT_READ|PROT_WRITE,
                  MAP_SHARED,
                  mem_fd,
                  0x20000000  //base  -> in physical address we looking at 0x20nnnnnn for 0x7Ennnnnn in bus address
              );

    
    if ((int)allof7e==-1) exit(-1);
  //  printf("\n");
  //  printf("%d",(int)allof7e); 
  //  printf("\n");

    SETBIT(GPFSEL0 , 14);   // GPIO pin 4 setting pin high
    CLRBIT(GPFSEL0 , 13); 	// GPIO pin 32
    CLRBIT(GPFSEL0 , 12);	// GPIO pin 33 
 
    struct GPCTL setupword = {6/*SRC*/, 1, 0, 0, 0, 1,0x5a};

    ACCESS(CM_GP0CTL) = *((int*)&setupword);
}


void modulate(int m)
{
    ACCESS(CM_GP0DIV) = (0x5a << 24) + 0x400+0x63000 + m; // 0x63000 gives us 5 Mhz
    //ACCESS(CM_GP0DIV) = (0x5a << 24) + 0x400+0x5000 + m;
  
  
    // ACCESS(CM_GP0DIV) = (0x5a << 24) + 0x341+0x21000 + m; // for 15, divide 500 by  33.33333 or 0x21341
    // 5a = the password
    // 0x4d72 -> original value
    // to down sample to 10MHz simply divide base clock by 50 (49 in DIVI and 1024 in DIVF)
}

void modulate2(int centerFreq, int m)
{
  int centerFreqDivider = (int)((500.0 / centerFreq) * (float)(1<<12) + 0.5);
  ACCESS(CM_GP0DIV) =(0x5a << 24) + centerFreqDivider+m;
  
}


void modulate4(float centerFreq)
{
  int cen1 = floor(((500.0 / (centerFreq))));
  cen1 = (cen1<<12); 
  int temp  = round((500.0 / (centerFreq)-floor(500.0 / (centerFreq)))*1024);
  int centerFreqDivider = temp+cen1;
  //printf("%s%d%s","\ndivider= ",temp,"\n");
  //printf("%s%d%s","\ndivider= ",cen1,"\n");
  //printf("%s%d%s","\ndivider= ",centerFreqDivider,"\n");
  ACCESS(CM_GP0DIV) =(0x5a << 24) + centerFreqDivider;
}

char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}

int main()
{	
  float centerFreq = 1.0;
  setup_fm(); 
  modulate4(centerFreq);
  printf("\%d\n", centerFreq);
  char ch;
  printf("Press a to increase the center frequency, press z to decrease\n");
  printf("centerFreq:\%f\n", centerFreq);
  do {
    ch=getch();
    //scanf("%c", &ch);
    switch (ch)
        {
            case 'a':
                centerFreq+=0.000001;
                modulate4(centerFreq);
                printf("centerFreq:\%f\n", centerFreq);
                break;

            case 'z':
                centerFreq-=0.000001;
                modulate4(centerFreq);
                printf("centerFreq:\%f\n", centerFreq);
                break; 
	    case 's':
                centerFreq+=0.0001;
                modulate4(centerFreq);
                printf("centerFreq:\%f\n", centerFreq);
                break;

            case 'x':
                centerFreq-=0.0001;
                modulate4(centerFreq);
                printf("centerFreq:\%f\n", centerFreq);
                break;
	   case 'd':
                centerFreq+=0.1;
                modulate4(centerFreq);
                printf("centerFreq:\%f\n", centerFreq);
                break;

            case 'c':
                centerFreq-=0.1;
                modulate4(centerFreq);
                printf("centerFreq:\%f\n", centerFreq);
                break;

        }
        
  } while (ch != '.'); 
 // int centerFreqDivider = (int)((500.0 / centerFreq) * (float)(1<<12) + 0.5);
 // printf("%d", centerFreqDivider); 
  //modulate2(centerFreq,0);
  //setupDMA(1.0);
   

}
