/*
** listener.c -- a datagram sockets "server" demo
*/
//udp includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

//spi includes
#include <bcm2835.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

//test queue includes
//#include <wiringPi.h>

// Port that the Pi will listen on to get UDP packets
#define MYPORT "9888"	

//Max number of bytes that the Pi will read per UDP packet
#define MAXBUFLEN 4096

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

//SPI control functions
void setupSPI()
{
	bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      	// <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   	// <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); 								// <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      	// <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      	// <-- I don't know why we have to set it low though. 
    
 }
  void closeSPI()
 {
 	bcm2835_spi_end();
    bcm2835_close();
 }

 void setup4MPCdac()
 {
 	// setting pin 25 as an output. 	<-- use only for the MCP DAC
    bcm2835_gpio_fsel(25, 1); 
 }




//Function to set this instance to FIFO high priority thread.
void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 0;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}


//Thread safe queue structure
typedef struct Queue
{
        int capacity;
        int size;
        int front;
        int rear;
        int *elements;
}Queue;

Queue * createQueue(int maxElements)
{
        /* Create a Queue */
        Queue *Q;
        Q = (Queue *)malloc(sizeof(Queue));
        /* Initialise its properties */
        Q->elements = (int *)malloc(sizeof(int)*maxElements);
        Q->size = 0;
        Q->capacity = maxElements;
        Q->front = 0;
        Q->rear = -1;
        /* Return the pointer */
        return Q;
}

void Dequeue(Queue *Q)
{
        /* If Queue size is zero then it is empty. So we cannot pop */
        if(Q->size==0)
        {
                //printf("Queue is Empty\n");
        		//printf("U");
        		//fflush(1); 
        		//usleep(100);
                return;
        }
        /* Removing an element is equivalent to incrementing index of front by one */
        else
        {
                Q->size--;
                Q->front++;
                /* As we fill elements in circular fashion */
                if(Q->front==Q->capacity)
                {
                        Q->front=0;
                }
        }
        return;
}

int front(Queue *Q)
{
        if(Q->size==0)
        {
                //printf("Queue is Empty\n");
                //printf("U");
                //fflush(1); 
        		//usleep(100);
                return 0;
        }
        /* Return the element which is at the front*/
        return Q->elements[Q->front];
}

void Enqueue(Queue *Q,int element)
{
        /* If the Queue is full, we cannot push an element into it as there is no space for it.*/
        if(Q->size == Q->capacity)
        {
                printf("ERROR: Cannot accept new data value, queue is full\n");
        }
        else
        {
                Q->size++;
                Q->rear = Q->rear + 1;
                /* As we fill the queue in circular fashion */
                if(Q->rear == Q->capacity)
                {
                        Q->rear = 0;
                }
                /* Insert the element in its rear side */ 
                Q->elements[Q->rear] = element;
        }
        return;
}

//Threadable function to read data from queue and write to SPI driver
void* transmitThread(void *arg)
{
	
	setupSPI();

	Queue *dataQueue = (Queue *)arg;
 
	uint8_t a[2] ={0x81,0xFF};        // 0x01 for external reference 
    uint8_t a2[2]={0xA4,0xFF};        // 0xA4 for external reference 
    uint8_t data[2] ={0}; 
    uint8_t b = 0x00;
    uint8_t b2= 0x00;
    uint32_t c = 0; 
    uint16_t d;

    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output.
    printf("LOG: SPI setup complete.\n");
    int flag = 1;

    while(flag){
		bcm2835_gpio_clr(25);
        bcm2835_spi_writenb(a,2); 
        bcm2835_gpio_set(25); 
        bcm2835_gpio_clr(25);
        bcm2835_spi_writenb(a2,2); 
        bcm2835_gpio_set(25);
        //b =  front(dataQueue);
        //a[1] =  b; 
        //a2[1]=  b;
        a[1]=front(dataQueue);
        a2[1]=front(dataQueue);
		Dequeue(dataQueue);
        //delayMicroseconds(99);
        usleep(1000);
	}

	closeSPI();
	printf("LOG: Closing SPI Thread.\n");
    return NULL;
    
}

//since this is run on a single core, the SPI thread hogs the CPU most of the time, and only occasionally switches back to the main thread to srvice the UDP packet reception.
int main(void)
{

	setRealtimeThread(); 
	if (!bcm2835_init()) // if that shizz fails... Panic!
	    return -1;

	//create FIFO queue structure
	Queue *sampleQueue = createQueue(MAXBUFLEN*100);
    
	//launch SPI control thread thread
	pthread_t tid;
	int err;
    err = pthread_create(&(tid), NULL, &transmitThread, sampleQueue);
    if (err != 0)
        printf("ERROR: can't create thread :[%s]", strerror(err));
    else
        printf("LOG: SPI thread created successfully\n");

    //init netowrk connections to listen for UDP packets
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int rv;
	int numbytes;
	struct sockaddr_storage their_addr;
	char buf[MAXBUFLEN];
	socklen_t addr_len;
	char s[INET6_ADDRSTRLEN];
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	printf("\n Queue thread variables set\n");
	rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo);
	if (rv != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("listener: bind");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	freeaddrinfo(servinfo);
	printf("LOG: Queue thread setup complete\n");
	int k = 0;
	while(1){

		addr_len = sizeof their_addr;
		if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN-1 , 0,
			(struct sockaddr *)&their_addr, &addr_len)) == -1) {
			perror("recvfrom");
			exit(1);
		}
		//add every byte from the UDP packet into the queue
		for (k = 0; k < numbytes; k++)
    	{
        	Enqueue(sampleQueue,(uint8_t)buf[k]);
    	}
	}
	close(sockfd);
	return 0;
}
