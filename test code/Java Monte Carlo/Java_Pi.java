import 	java.lang.Math;
import java.util.Random;

public class Java_Pi
{
	private static Random randomGenerator = new Random();
	private static double Pi_calc(double[] rand1, double[] rand2, long iterations)
	{
		double random1 =0.0; 
		double random2 =0.0; 
		double counter =0;
		for (int i=0; i<iterations; i++)
    	{
    		//random1 = Math.random(); 
    		//random2 = Math.random(); 
    		random1 = rand1[i]; 
    		random2 = rand2[i]; 
			if (Math.sqrt(Math.pow(random1,2)+Math.pow(random2,2))<=1)
			{
				counter = counter +1.0;
			}
		}

		return counter; 
	}

	private static double[] randFill(int iterations)
	{
		double[] rand; 
		rand = new double[iterations]; 
		for(int i=0; i<iterations; i++)
		{
			//rand[i] = Math.random(10); 
			rand[i] = randomGenerator.nextDouble(); 
		}
		return rand; 
	}
	
	public static void main (String args[])
	{
		long seed =10;
		int iterations =500000; 
		randomGenerator.setSeed(seed);
		double counter = 0;
		long startTime=0;
    	long endTime=0;
    	for (int i = 1; i<11; i++){
    		double[] rand1 = randFill(iterations*i);
    		double[] rand2 = randFill(iterations*i);


			startTime = System.currentTimeMillis();
	    	counter = Pi_calc(rand1,rand2, iterations*i);
	    	endTime= System.currentTimeMillis();


	    	double pi_est = (counter*4)/(iterations*i); 
	      	System.out.format("The total time spent is: %d ms, with estimated Pi being: %.10f, using %d iterations%n",(endTime-startTime),pi_est,iterations*i);
      }
	}
}