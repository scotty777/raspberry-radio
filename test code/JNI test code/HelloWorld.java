class HelloWorld {
	public native void print(); //native method
	public native void runPiFM(); //native method
	static{				//static init code
		System.loadLibrary("HelloWorld"); //Loads CLibHelloWorld.dll
	} 

	public static void main(String[] args){
		HelloWorld hw = new HelloWorld();
		hw.print();
		hw.runPiFM();
	}

}