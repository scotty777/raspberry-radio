#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
using namespace std; 


	double Pi_calc(long iterations)
	{
		double random1 =0.0; 
		double random2 =0.0; 
		double counter =0;
		srand(10);
		for (long i=0; i<=iterations; i++)
    	{

    		random1 =  static_cast<double>(rand()) / RAND_MAX;
    		random2 =  static_cast<double>(rand()) / RAND_MAX;
			if (sqrt(pow(random1,2)+pow(random2,2))<=1)
			{
				counter = counter +1.0;
			}
		}

		return counter; 
	}

	int main ()
	{
		long iterations =500000; 
		clock_t begin, end; 
		double time_spent =0;
		double counter = 0;
		for (int i = 1; i<3; i++)
    	{
			begin  = clock();
	    	counter = Pi_calc(iterations*i);
	    	end  = clock(); 
	    	time_spent= (double)(end - begin)/ CLOCKS_PER_SEC; 
	    	double pi_est = (counter*4)/(iterations*i); 
      }
    	for (int i = 1; i<11; i++)
    	{
			begin  = clock();
	    	counter = Pi_calc(iterations*i);
	    	end  = clock(); 
	    	time_spent= (double)(end - begin)/ CLOCKS_PER_SEC; 
	    	double pi_est = (counter*4)/(iterations*i); 
	      	cout<<"The total time spent is: "<<time_spent<<"ms, with estimated Pi being: "<<pi_est<<",using "<<iterations*i<<endl; 
      }
	}