public class prime {

  static int prime_counter(int iterations)
  {
    int counter =1;
    for (int i=3;i<=iterations;i++) {
      boolean prime = true;
      for (int j=2;j<i;j++) {
        int k = i/j;
        int l = k*j;
        if (l==i) prime = false;
      }
      if (prime) counter++;
    }

    return counter;
  }


  public static void main(String [] args) {
    int counter = 1;
    long startTime=0;
    long endTime=0;
    int mult = 0; 

    for(int i=1; i<10; i++){
      mult = i*5000; 
      startTime = System.currentTimeMillis();
      counter = prime_counter(mult);
      endTime= System.currentTimeMillis();
      System.out.println("The total time spent is: " + (endTime-startTime)+"ms, with total primes being: "+counter +", with "+mult+" numbers searched");
    }
  }
}