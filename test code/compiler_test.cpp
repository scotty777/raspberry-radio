#include <iostream>
#include <time.h>
using namespace std;

int main ()
{

    clock_t begin, end;
    double time_spent;

    begin = clock();
    
    
    cout << "Starting loop"<<endl;
    long double a = 1;
    for(int i = 0; i < 90000000; i++){
        a = a*0.1+1;
    }
    cout<< "Result = "<<a<<endl;
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    cout << "Time taken: "<<time_spent<<endl;
    return 0;
}