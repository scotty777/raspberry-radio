#include  <iostream>
#include <time.h>
using namespace std; 



int prime_counter(int iterations)
{
  int counter =1; 
   for (int i=3;i<=iterations;i++) {
      int prime = 1;
      for (int j=2;j<i;j++) {
        int k = i/j;
        int l = k*j;
        if (l==i) prime = 0;
      }
      if (prime) counter++;
    }

    return counter; 
}


int main() {

  clock_t begin, end; 
  int counter=1; 
  double time_spent =0;
  int mult =0;


  for (int i=1; i<10; i++){
    begin  = clock(); 
    mult = i*5000; 
    counter= prime_counter(mult); 
    end  = clock(); 

    time_spent= (double)(end - begin)/ CLOCKS_PER_SEC; 

    cout<<"The total time spent is: "<<time_spent<<"s, with total primes being: "<<counter<<", with "<<mult<<" numbers searched "<<endl; 
  } 

}