
#include <bcm2835.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <time.h>


void handSig() {
    exit(0);
}

uint32_t writeable; 
bool want_to_write;  
uint32_t readble; 

void spi_read_write()
{
	if(BCM2835_SPI0_CS_RXR )
	{
		readable = bcm2835_peri_read_nb(fifo); 	// reads data from the fifo 
	}

	if(BCM2835_SPI0_CS_DONE&&want_to_write)		// if we want to write and the done bit is high, then lets write
	{
		volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    	volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;

    	bcm2835_peri_write_nb(fifo, writeable);	// we want to write with no barrier -> as fast as possible.
    	want_to_write =0; 
	}
	else if(BCM2835_SPI0_CS_DONE&&!want_to_write)
	{
		bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
	}
}


void setupInterupt()
{
	signal(SIGIO, spi_read_write); 
	signal(SIGPOLL, spi_read_write); 
  	signal (SIGINT, handSig);
  	signal (SIGTERM, handSig);
  	signal (SIGHUP, handSig);
  	signal (SIGQUIT, handSig);
	bcm2835_peri_set_bits(paddr,1, BCM2835_SPI0_CS_INTR);
    bcm2835_peri_set_bits(paddr, 1, BCM2835_SPI0_CS_INTD);
}



// Writes an number of bytes to SPI

void bcm2835_spi_write4b(char* tbuf, uint32_t len)
{
    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;

    // This is Polled transfer as per section 10.6.1
    // BUG ALERT: what happens if we get interupted in this section, and someone else
    // accesses a different peripheral?

    // Clear TX and RX fifos
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_CLEAR, BCM2835_SPI0_CS_CLEAR);
    //Set TA =1 (high)
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_TA, BCM2835_SPI0_CS_TA);

    uint32_t i;
  for (i = 0; i < len; i++)
  {
    // Maybe wait for TXD
    while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_TXD))
      ;

    // Write to FIFO, no barrier
    bcm2835_peri_write_nb(fifo, tbuf[i]);
  }

    // Wait for DONE to be set
    while (!( BCM2835_SPI0_CS_DONE))//{  // what happens on received signal if the CS_Done bit is not waited
        //usleep(1); }
      ;

    // Set TA = 0, and also set the barrier
    bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
}



void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 0;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
 
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}

int main()
{
    setRealtimeThread(); 
	if (!bcm2835_init()) // if that shizz fails... 
	return -1;

    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(8); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 

    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;

    uint8_t a[4] ={0xFF,0xFF,0xFF,0xFF};
    uint8_t data[4] ={0}; 

    uint8_t b = 0xFF;

    // uint32_t data2 = (uint32_t)a[0]<<8+(uint32_t)a[1]<<8+(uint32_t)a[2]<<8+(uint32_t)a[3]; 
    

   // for(int i =0; i<10; i++)
    while(1==1)
    {
        //printf("Read from SPI: %02X%02X\n", data[0],data[1]);

        bcm2835_spi_write4b(a,4);                               // This writes as many bytes as we want to the SPI (usuful for a DAC)
       // bcm2835_spi_transfernb(a,data,32);                         // this sends 2 bytes and receives 2 bytes. This will be useful for the DAC (control byte+data byte)
       //data = bcm2835_spi_transfer(a++);
        //bcm2835_spi_writenb(a,16);
       // usleep(1); 
        //bcm2835_delayMicroseconds(1);
        
    }

    bcm2835_spi_end();
    bcm2835_close();
}