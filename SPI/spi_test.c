//#include "spi.h"
#include <bcm2835.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

void setupSPI()
{
	bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 
    
 }
  void closeSPI()
 {
 	bcm2835_spi_end();
    bcm2835_close();
 }
 void setup4MPCdac()
 {
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 	<-- use only for the MCP DAC
 }
int main()
{
   // setRealtimeThread(); 
	  if (!bcm2835_init()) // if that shizz fails... 
	    return -1;
/*
    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 

    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;
	*/
	setupSPI();
    uint8_t a[2] ={0x3F,0xFF};
    uint8_t data[2] ={0}; 

    uint8_t b = 0x00;
    //uint8_t c = 0x30; 
    uint32_t c = 0; 
    uint16_t d;

    
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 
    long i =0;
   while (1)
    {
        bcm2835_gpio_clr(25);
        bcm2835_spi_writenb(a,2); 
        bcm2835_gpio_set(25); 
        b = (uint8_t)(127*sin(2*3.141*i/10000)+128); 
        i++; 
        a[1] =  (b); 
    }
    closeSPI();
  //  bcm2835_spi_end();
  //  bcm2835_close();
}
