#include <bcm2835.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

void setupSPI()
{
	bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(256); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 
    
 }
  void closeSPI()
 {
 	bcm2835_spi_end();
    bcm2835_close();
 }
 void setup4MPCdac()
 {
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 	<-- use only for the MCP DAC
 }



// Writes (and reads) an number of bytes to SPI
void spi_readnb( char *tbuf, char* rbuf, uint32_t len)
{
    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;
    

    // This is Polled transfer as per section 10.6.1
    // BUG ALERT: what happens if we get interupted in this section, and someone else
    // accesses a different peripheral? 

    // Clear TX and RX fifos
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_CLEAR, BCM2835_SPI0_CS_CLEAR);

    // Set TA = 1
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_TA, BCM2835_SPI0_CS_TA);

    uint32_t i;
    for (i = 0; i < len; i++)
    {
    // Maybe wait for TXD
    while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_TXD))
        delayMicroseconds(10);
  //  printf("%s", "sending out the data\n");
    // Write to FIFO, no barrier
    bcm2835_peri_write_nb(fifo, tbuf[i]);

    // Wait for RXD
    while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_RXD))
        delayMicroseconds(10);
  //  printf("%s", "reading in the data\n");
    // then read the data byte
    rbuf[i] = bcm2835_peri_read_nb(fifo);
    }
    // Wait for DONE to be set
    while (!(bcm2835_peri_read_nb(paddr) & BCM2835_SPI0_CS_DONE))
    delayMicroseconds(10);

    // Set TA = 0, and also set the barrier
    bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
}

void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 1;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
 
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}


int main()
{
  //  setRealtimeThread(); 
	  if (!bcm2835_init()) // if that shizz fails... 
	    return -1;

    setRealtimeThread();  // setting the thread priority

	setupSPI();            // setting up the SPI 

    const int len = 2; 

    uint8_t a[2] ={0x00,0x00};  // we only want to send zeros to the ADC -> since we aren't actually going to be 
                                            // sending anything to the ADC, there's no pin for that, so this can all be set high, 
                                            // but we might as well just set it all low and save power. 
    uint8_t b[2] ={0x00,0x00}; 
    uint8_t test_data; 
    uint16_t data_A = 0;    // Data from ADC A
    uint16_t data_B = 0;    // Data from ADC B
    uint16_t temp =0;          // temp value for the data handling procedure 

    float DataA = 0.0;         // the raw voltage 'seen' by the ADC at channel A
    float DataB = 0.0;         // the raw voltage 'seen' by the ADC at channel B
    
    const float volts_per_bit = 5/4096.0; // this is the data conversion that we will be using.
    bcm2835_gpio_fsel(25, 1);  // setting pin 25 as an output. 
    long i =0;

   while (1)
    {
        
      //  printf("%s", "begining the voltage reading process\n");
      //  bcm2835_gpio_clr(25);   // pin 25 will be our chip selector -> setting to zero will turn on the whole process
        // a is a 4 byte (32 bit) value that will be sent and received from the SPI module 
        // we are sending 32 bits since we need to hold the control pin low for two full data transfers 
        // allowing for both values from the ADC channels to be read simulatiously. This is essential since we need to receive 
        // both ADC values at the same time instance to prevent phase shifts in the data (this is especially important for digital tranmission)
        bcm2835_spi_chipSelect(0);
        spi_readnb(a,a, len); 
        bcm2835_spi_chipSelect(1);
        spi_readnb(b,b, len); 

      /*  printf("%s%d%s", "The bytes from a[0]", a[0], "\n");
        printf("%s%d%s", "The bytes from a[1]", a[1], "\n");
        printf("%s%d%s", "The bytes from b[0]", b[0], "\n");
        printf("%s%d%s", "The bytes from b[1]", b[1], "\n");
*/
        // A channel
        test_data = a[0]<<3; 
        test_data = test_data>>3; 
        temp = (uint16_t)test_data; 
        data_A = temp<<8; 
        test_data = a[1]; 
        temp = (uint16_t)test_data; 
        data_A = (data_A+test_data)>>1; 
        // B Channel 
        test_data = b[0]<<3; 
        test_data = test_data>>3; 
        temp = (uint16_t)test_data; 
        data_B = temp<<8; 
        test_data = a[1]; 
        temp = (uint16_t)test_data; 
        data_B = (data_B+test_data)>>1; 
      //  printf("%s%d%s", "The bytes from B: ", data_B, "\n");
/*
        test_data = a[0]<<3; 
        test_data = test_data>>3; 
     

        temp = (uint16_t)(test_data); 
        data_A = temp<<8;            // shifting the MSB up by 4 spots, since 8+4 = 12, so the MSB is in the 12th bit of the 16 bit int

        temp = (uint16_t)(a[1]); 
       // temp = temp>>4;              // since in the second byte, 4 bits are zeros( at the end), we need to remove them, 
                                     // so shift the data 4 times to the right  
        data_A = (data_A+temp)>>4;        // saving all the data int the 16-bit integer

        temp =0; 
*/
        DataA = data_A*volts_per_bit; // converting binary to a voltage value (that's actually relevant ) for channel A
/*

        test_data = b[0]<<3; 

        test_data = test_data>>3; 

        temp = (uint16_t)(test_data); 
        data_B = temp<<8;            // shifting the MSB up by 4 spots, since 8+4 = 12, so the MSB is in the 12th bit of the 16 bit int
    
        temp = (uint16_t)(b[1]); 
       // temp = temp>>4;              // since in the second byte, 4 bits are zeros( at the end), we need to remove them, 
                                     // so shift the data 4 times to the right  
        data_B = (data_B+temp)>>4;        // saving all the data int the 16-bit integer

        temp =0; 
*/
        DataB = (float)(data_B)*volts_per_bit; // converting binary to a voltage value (that's actually relevant ) for channel A

        printf("%s%f%s", "voltage reading from A: ", DataA, "\n");  // lets print out the voltage measured from the ADC.
        printf("%s%f%s", "voltage reading from B: ", DataB, "\n");  // From both data ports. 
      
        // flushing out a for the next data transfer. -> set low for the luxury of not wasting power. 
        a[0] = 0x00; 
        a[1] = 0x00;  
        b[0] = 0x00; 
        b[1] = 0x00;
    
        // setting to 0.5 second of sleep. 
        usleep(50000);         // just setting to 100 usleep for this -> allows for the the RPi to sleep every so often, and lowers the power. 
    }
    closeSPI();

}
