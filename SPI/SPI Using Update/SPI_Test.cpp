#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <bcm2835.h>
#include <cmath>

 
class mcp3008Spi{
 
public:
    mcp3008Spi();
    mcp3008Spi(std::string devspi, unsigned char spiMode, unsigned int spiSpeed, unsigned char spibitsPerWord);
    ~mcp3008Spi();
    int spiWriteRead( unsigned char *data, int length);
    int spiWriteDAC(unsigned char *data, int length); 
 
private:
    unsigned char mode;
    unsigned char bitsPerWord;
    unsigned int speed;
    int spifd;
 
    int spiOpen(std::string devspi);
    int spiClose();
 
};

using namespace std;
/**********************************************************
 * spiOpen() :function is called by the constructor.
 * It is responsible for opening the spidev device 
 * "devspi" and then setting up the spidev interface.
 * private member variables are used to configure spidev.
 * They must be set appropriately by constructor before calling
 * this function. 
 * *********************************************************/
int mcp3008Spi::spiOpen(std::string devspi){
    //bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 
    int statusVal = -1;
    this->spifd = open(devspi.c_str(), O_RDWR);
    if(this->spifd < 0){
        perror("could not open SPI device");
        exit(1);
    }
 
    statusVal = ioctl (this->spifd, SPI_IOC_WR_MODE, &(this->mode));
    if(statusVal < 0){
        perror("Could not set SPIMode (WR)...ioctl fail");
        exit(1);
    }
 
    statusVal = ioctl (this->spifd, SPI_IOC_RD_MODE, &(this->mode)); 
    if(statusVal < 0) {
      perror("Could not set SPIMode (RD)...ioctl fail");
      exit(1);
    }
 
    statusVal = ioctl (this->spifd, SPI_IOC_WR_BITS_PER_WORD, &(this->bitsPerWord));
    if(statusVal < 0) {
      perror("Could not set SPI bitsPerWord (WR)...ioctl fail");
      exit(1);
    }
 
    statusVal = ioctl (this->spifd, SPI_IOC_RD_BITS_PER_WORD, &(this->bitsPerWord));
    if(statusVal < 0) {
      perror("Could not set SPI bitsPerWord(RD)...ioctl fail");
      exit(1);
    }   
 
    statusVal = ioctl (this->spifd, SPI_IOC_WR_MAX_SPEED_HZ, &(this->speed));     
    if(statusVal < 0) {
      perror("Could not set SPI speed (WR)...ioctl fail");
      exit(1);
    } 
 
    statusVal = ioctl (this->spifd, SPI_IOC_RD_MAX_SPEED_HZ, &(this->speed));     
    if(statusVal < 0) {
      perror("Could not set SPI speed (RD)...ioctl fail");
      exit(1);
    }
    return statusVal;
}
 
/***********************************************************
 * spiClose(): Responsible for closing the spidev interface.
 * Called in destructor
 * *********************************************************/
 
int mcp3008Spi::spiClose(){
    int statusVal = -1;
    statusVal = close(this->spifd);
        if(statusVal < 0) {
      perror("Could not close SPI device");
      exit(1);
    }
    return statusVal;
}
 
/********************************************************************
 * This function writes data "data" of length "length" to the spidev
 * device. Data shifted in from the spidev device is saved back into 
 * "data". 
 * ******************************************************************/
int mcp3008Spi::spiWriteRead( unsigned char *data, int length){
 
  struct spi_ioc_transfer spi[length];
  int i = 0; 
  int retVal = -1;  
 
// one spi transfer for each byte
 
  for (i = 0 ; i < length ; i++){
 
    spi[i].tx_buf        = (unsigned long)(data + i); // transmit from "data"
    spi[i].rx_buf        = (unsigned long)(data + i) ; // receive into "data"
    spi[i].len           = sizeof(*(data + i)) ;
    spi[i].delay_usecs   = 0 ; 
    spi[i].speed_hz      = this->speed ;
    spi[i].bits_per_word = this->bitsPerWord ;
    spi[i].cs_change = 0;
}


 
 retVal = ioctl (this->spifd, SPI_IOC_MESSAGE(length), &spi) ;
 
 if(retVal < 0){
    perror("Problem transmitting spi data..ioctl");
    exit(1);
 }
 
return retVal;
 
}


int mcp3008Spi::spiWriteDAC( unsigned char *data, const int length){
 
  struct spi_ioc_transfer spi[length];

  int i = 0; 
  int retVal = -1;  
 
// one spi transfer for each byte

  
  for (i = 0 ; i < length ; i++)
  {
    spi[i].tx_buf        = (unsigned long)(data+i); // transmit from "data"
    spi[i].rx_buf        = (unsigned long)(data+i) ; // receive into "data"
    spi[i].len           = sizeof(*(data+i));
    spi[i].delay_usecs   = 0 ; 
    spi[i].speed_hz      = this->speed ;
    spi[i].bits_per_word = this->bitsPerWord ;
    spi[i].cs_change     = 0;
} 


 retVal = ioctl (this->spifd, SPI_IOC_MESSAGE(length), &spi) ;

 if(retVal < 0){
    perror("Problem transmitting spi data..ioctl");
    exit(1);
 }
 
return retVal;
 
}
 
/*************************************************
 * Default constructor. Set member variables to
 * default values and then call spiOpen()
 * ***********************************************/
 
mcp3008Spi::mcp3008Spi(){
    this->mode = SPI_MODE_0 ; 
    this->bitsPerWord = 8;
    this->speed = 1000000;
    this->spifd = -1;
 
    this->spiOpen(std::string("/dev/spidev0.0"));
 
    }
 
/*************************************************
 * overloaded constructor. let user set member variables to
 * and then call spiOpen()
 * ***********************************************/
mcp3008Spi::mcp3008Spi(std::string devspi, unsigned char spiMode, unsigned int spiSpeed, unsigned char spibitsPerWord){
    this->mode = spiMode ; 
    this->bitsPerWord = spibitsPerWord;
    this->speed = spiSpeed;
    this->spifd = -1;
    this->spiOpen(devspi);
 
}
 
/**********************************************
 * Destructor: calls spiClose()
 * ********************************************/
mcp3008Spi::~mcp3008Spi(){
    this->spiClose();
}



void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 1;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
 
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}

 
int main(void)
{

    setRealtimeThread();
    mcp3008Spi a2d("/dev/spidev0.0", SPI_MODE_0, 32000, 8);
    long i = 1;
    if (!bcm2835_init()) // if that shizz fails... 
        return -1;

    bcm2835_gpio_fsel(25, 1);
     //  int a2dVal = 0; 
    //int a2dChannel = 0;
    unsigned char con_4_A = 0b00000001;
    unsigned char con_4_B = 0b00100100;

    const int resolution =20;   // this is our resolution towards how many samples/period
    const int bytes_per_sample = 4; 
    const int periods_per_sample =1000;
    
   // unsigned char data[resolution*periods_per_sample*bytes_per_sample] ={0}; // this will have 10 waves, and 4 bytes per wave

  /*  for(int i =0; i<resolution*periods_per_sample; i++)
    {
       data[i*4]   = con_4_A; 
       data[i*4+1] = (unsigned char)(127*sin(2*3.141*i/resolution)+128);
       data[i*4+2] = con_4_B; 
       data[i*4+3] = (unsigned char)(127*cos(2*3.141*i/resolution)+128);

   }
*/

  //  unsigned char temp[2];

    unsigned char data[2];
    unsigned char data2[2] = {0};
    cout<<"Starting the loop\n"; 
    while(i > 0 )
    {
        data[0]   = 0b00000001; 
        data[1]   = (unsigned char)(127*sin(2*3.141*i/20)+128);
        //cout<<data[0]<<" \n";
        //data[1]   = 0b01111000; 
        //cout<<data[1]<<" \n";
       // data[0] = 1;  //  first byte transmitted -> start bit
       // data[1] = 0b10000000 |( ((a2dChannel & 7) << 4)); // second byte transmitted -> (SGL/DIF = 1, D2=D1=D0=0)
      //  data[2] = 0; // third byte transmitted....don't care
      //  bcm2835_gpio_clr(25);

       // for (int j =0; j<2*resolution*periods_per_sample; j++){
        //    temp[0] = data[j*2]; 
        //    temp[1] = data[j*2+1]; 
            a2d.spiWriteDAC(data,2);
      //  }   
     //  bcm2835_gpio_set(25);
       data2[0]   = 0b00100100; 
        data2[1]   = (unsigned char)(127*cos(2*3.141*i/20)+128);
        
       // bcm2835_gpio_clr(25);
        a2d.spiWriteDAC(data2, 2); 
       // bcm2835_gpio_set(25);
        //usleep(1); 
         
 /*    
        a2dVal = 0;
                a2dVal = (data[1]<< 8) & 0b1100000000; //merge data[1] & data[2] to get result
                a2dVal |=  (data[2] & 0xff);
        sleep(1);
        cout << "The Result is: " << a2dVal << endl;
        i--; */

        i++; 
    }
    return 0;
}