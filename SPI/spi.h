/*
To use this code, simply include the file, all the headers will be included in this header, so you don't have to. 

Call setupSPI() to setup the right files,
once done with the program, call closeSPI(); 



to write to SPI, simply use:
bcm2835_write_nb(bytes,lengthofbytes)  

to transfer and read simply use: 
bcm2835_transfer_nb(trans_bytes,rec_bytes, length) 

*/
#include <bcm2835.h>


/*
const volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
const volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;
*/
void setupSPI()
{
	bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 
    
 }
 void setup4MPCdac()
 {
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 	<-- use only for the MCP DAC
 }

 void writeSPIMPC(uint8_t *buff, int len)		// this just writes to the spi using the MCP DAC
 {
 	bcm2835_gpio_clr(25);
    bcm2835_spi_writenb(buff,len); 
    bcm2835_gpio_set(25); 

 }

 void closeSPI()
 {
 	bcm2835_spi_end();
    bcm2835_close();
 }