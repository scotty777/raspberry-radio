#include <bcm2835.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

void setupSPI()
{
	bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 
    
 }
  void closeSPI()
 {
 	bcm2835_spi_end();
    bcm2835_close();
 }
 void setup4MPCdac()
 {
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 	<-- use only for the MCP DAC
 }


void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 1;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
 
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}



int main()
{
    setRealtimeThread(); 
	  if (!bcm2835_init()) // if that shizz fails... 
	    return -1;
/*
    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_lsMODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 

    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;
	*/
	setupSPI();


   //uint8_t a[4] ={0x3F,0xFF,0x24,0xFF};
    //uint8_t a[2] ={0x01,0xFF};      // 0x01 for internal reference 
    //uint8_t a2[2]={0x24,0xFF};      // 0x24 for internal reference
    uint8_t a[2] ={0x81,0xFF};        // 0x01 for external reference 
    uint8_t a2[2]={0xA4,0xFF};        // 0xA4 for external reference 

    uint8_t data[2] ={0}; 

    uint8_t b = 0x00;
    uint8_t b2= 0x00;
    //uint8_t c = 0x30; 
    uint32_t c = 0; 
    uint16_t d;

    
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 
    long i =0;
    int j =0;
   while (1)
    {
        for(j=0; j<1000;j++){

        bcm2835_gpio_clr(25);
        bcm2835_spi_writenb(a,2); 
        bcm2835_gpio_set(25); 
       // usleep(1); 
        bcm2835_gpio_clr(25);
        bcm2835_spi_writenb(a2,2); 
        bcm2835_gpio_set(25); 
<<<<<<< HEAD
        b = (uint8_t)(127*sin(2*3.141*i/150)+128); 
=======
        b = (uint8_t)(127*sin(2*3.141*i/50)+128); 
>>>>>>> 7116d2dbe7ce87560386c2f982fdbf61bd2a477f
     //   b2= (uint8_t)(127*cos(2*3.141*i/15)+128);

        i++; 
        a[1] =  (b); 
        a2[1]=  (b);
       // usleep(1); 
    }
   // usleep(10000); 

    }
    closeSPI();
  //  bcm2835_spi_end();
  //  bcm2835_close();
}
