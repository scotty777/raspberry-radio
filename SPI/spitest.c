/* This is a test file that will show how to program on the PI using BCM2835 library
*
* to compile, use gcc -o test test.c -lbcm2835
* the library -lbcm2835 is important in the compilation process
*
*/

#include <bcm2835.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <math.h>

#define DMABASE (0x7E007000)  // the DMA Base address.
//#define DMABASE2 (0x7E007100)  // the DMA Base address.

volatile unsigned *allof7e;

#define ACCESS(base) *(volatile int*)((int)allof7e+base-0x7e000000)
#define SETBIT(base, bit) ACCESS(base) |= 1<<bit
#define CLRBIT(base, bit) ACCESS(base) &= ~(1<<bit)

// NOW We need to set up DMA to be used for this function
// Oh Joy

void getRealMemPage(void** vAddr, void** pAddr) {
    void* a = valloc(4096);
    
    ((int*)a)[0] = 1;  // use page to force allocation.
    
    mlock(a, 4096);  // lock into ram.
    
    *vAddr = a;  // yay - we know the virtual address
    
    unsigned long long frameinfo;
    
    int fp = open("/proc/self/pagemap", 'r');
    lseek(fp, ((int)a)/4096*8, SEEK_SET);
    read(fp, &frameinfo, sizeof(frameinfo));
    
    *pAddr = (void*)((int)(frameinfo*4096));
}

void freeRealMemPage(void* vAddr) {
    
    munlock(vAddr, 4096);  // unlock ram.
    
    free(vAddr);
}

// this is the control block for the DMA 
struct CB {
    volatile unsigned int TI;
    volatile unsigned int SOURCE_AD;
    volatile unsigned int DEST_AD;
    volatile unsigned int TXFR_LEN;
    volatile unsigned int STRIDE;
    volatile unsigned int NEXTCONBK;
    volatile unsigned int RES1;
    volatile unsigned int RES2;
    
};

struct DMAregs {
    volatile unsigned int CS;
    volatile unsigned int CONBLK_AD;
    volatile unsigned int TI;
    volatile unsigned int SOURCE_AD;
    volatile unsigned int DEST_AD;
    volatile unsigned int TXFR_LEN;
    volatile unsigned int STRIDE;
    volatile unsigned int NEXTCONBK;
    volatile unsigned int DEBUG;
};

struct PageInfo {
    void* p;    // physical address
    void* v;   // virtual address
};

struct PageInfo constPage;   
struct PageInfo instrPage;
struct PageInfo instrs;


void unSetupDMA(){
    printf("exiting\n");
    struct DMAregs* DMA0 = (struct DMAregs*)&(ACCESS(DMABASE));
    DMA0->CS =1<<31;  // reset dma controller
    
}
void setupDMA()
{
   atexit(unSetupDMA);
  // allocate a few pages of ram
   getRealMemPage(&constPage.v, &constPage.p); // the commands that will be sent to the DMA 

   // Now that we have constPage -> the DMA  
   // we need to load instuctions in it. 
  //((int*)(constPage.v)) = (0x5a << 24) + centerFreqDivider - 512 + i;



  //bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_ADCS,BCM2835_SPI0_CS_ADCS); // enabling DMA 
  // bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_DMAEN,BCM2835_SPI0_CS_DMAEN);// enabling DMA

   getRealMemPage(&instrPage.v, &instrPage.p);

   // make copy instructions -> this points to the instrPage, and is what we will write to. 
   struct CB* instr0= (struct CB*)instrPage.v;


   instrs.v = (void*)((int)instrPage.v + sizeof(struct CB));    // assigning points to 
   instrs.p = (void*)((int)instrPage.p + sizeof(struct CB));

   instr0->SOURCE_AD = (unsigned int)constPage.p+2048;           // what we will write to -> constPage holds all the instructions we will need.
   instr0->DEST_AD = 0x18 /* d FIF1 */;
   instr0->TXFR_LEN = 4;
   instr0->STRIDE = 0;
       //instr0->NEXTCONBK = (int)instrPage.p + sizeof(struct CB)*(i+1);
   instr0->TI = (1/* DREQ  */<<6) | (5 /* PWM */<<16) |  (1<<26/* no wide*/) ;
   instr0->RES1 = 0;
   instr0->RES2 = 0;

    //activate dma
   struct DMAregs* DMA1 = (struct DMAregs*)&(ACCESS(DMABASE+0x100));  // channel 1
   struct DMAregs* DMA2 = (struct DMAregs*)&(ACCESS(DMABASE+0x200));  // channel 2 

  // bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_DMAEN, BCM2835_SPI0_CS_DMAEN);
  // bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_ADCS, BCM2835_SPI0_CS_ADSC);
   DMA1->CS =1<<31;               // reset the DMA
   DMA1->CONBLK_AD=0; 
   DMA1->TI=0; 
   DMA1->CONBLK_AD = (unsigned int)(instrPage.p);
   DMA1->CS =(1<<0)|(255 <<16);  // enable bit = 0, clear end flag = 1, prio=19-16
}


// Writes an number of bytes to SPI
void bcm2835_spi_write4b(char* tbuf, uint32_t len)
{
    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;

    // This is Polled transfer as per section 10.6.1
    // BUG ALERT: what happens if we get interupted in this section, and someone else
    // accesses a different peripheral?

    // Clear TX and RX fifos
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_CLEAR, BCM2835_SPI0_CS_CLEAR);

    // Set TA = 1
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_TA, BCM2835_SPI0_CS_TA);

  //  bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_ADCS,BCM2835_SPI0_CS_ADCS);
  //  bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_DMAEN,BCM2835_SPI0_CS_DMAEN);

   // printf("Fuck the police"); 

    uint32_t i;
  for (i = 0; i < len; i++)
  {
    // Maybe wait for TXD
 //   while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_TXD))
   //   ;

    // Write to FIFO, no barrier
    bcm2835_peri_write_nb(fifo, tbuf[i]);
  }

    // Wait for DONE to be set
  //  while (!( BCM2835_SPI0_CS_DONE))//{  // what happens on received signal if the CS_Done bit is not waited
        //usleep(1); }
   //   ;

    // Set TA = 0, and also set the barrier
  //  bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
}

void endTrans()
{
  // settting TA =0; 
  volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
  bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
}






void bcm2835_spi_trans(char* tbuf, char* rbuf, uint32_t len)
{
    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;

    // This is Polled transfer as per section 10.6.1
    // BUG ALERT: what happens if we get interupted in this section, and someone else
    // accesses a different peripheral? 

    // Clear TX and RX fifos
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_CLEAR, BCM2835_SPI0_CS_CLEAR);

    // Set TA = 1
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_TA, BCM2835_SPI0_CS_TA);

    uint32_t i;
    for (i = 0; i < len; i++)
    {
  // Maybe wait for TXD
   //   while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_TXD))
      //delayMicroseconds(10)
//      ;

  // Write to FIFO, no barrier
  bcm2835_peri_write_nb(fifo, tbuf[i]);

  // Wait for RXD
  while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_RXD))
     // delayMicroseconds(10)
    ;

  // then read the data byte
  rbuf[i] = bcm2835_peri_read_nb(fifo);
    }
    // Wait for DONE to be set
  //  while (!(bcm2835_peri_read_nb(paddr) & BCM2835_SPI0_CS_DONE))  -->  user must be able to check if CS_DONE is high
 // delayMicroseconds(10)
//;

    // Set TA = 0, and also set the barrier
   // bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
}



void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 0;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
 
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}

int main()
{
    setRealtimeThread(); 
	  if (!bcm2835_init()) // if that shizz fails... 
	    return -1;

    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(32); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 

    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;

    uint8_t a[2] ={0x3F,0xFF};
    uint8_t data[2] ={0}; 

    uint8_t b = 0x00;
    //uint8_t c = 0x30; 
    uint32_t c = 0; 
    uint16_t d;

    // uint32_t data2 = (uint32_t)a[0]<<8+(uint32_t)a[1]<<8+(uint32_t)a[2]<<8+(uint32_t)a[3]; 
    
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 
    long i =0;
   while (1)
    {
        bcm2835_gpio_clr(25);
        bcm2835_spi_writenb(a,2); 
        bcm2835_gpio_set(25); 

        b = (uint8_t)(127*sin(2*3.141*i/10000)+128); 
        //c = (uint16_t)(2047*sin(2*3.141*i/1000)+2048);
        i++; 
       // d = (c<<8);
       // d = d>>8;
        a[1] =  (b); 
         

        //d = c<<12; 
      //  b = d>>12; 
        //a[0] = (uint8_t)b+0x30; 
       // printf("char is: %d\n",a[0]);

        //d = (uint16_t)b
       // d =d<<8;
        //d=d+(uint16_t)a[1]; 
      

      //  printf("char is: %d\n",d);
     //   *d = &c; 
    //    a[1] = d[3];
     //   a[0] = d[2];
     //   printf("%c",a[0]);

     //   b++; 
      //  a[1] =b;   
     //   if(b==0xFF){
     //     b =0x00; 
      //    a[1] = b; 
     //   }
         //c++; 
       // if(c=0x40){
       //   c=0x30; 
       // }
      //  a[0] =  c; 
      //  if(c==0x3F)
      //  {
      //    c=0x30; 
      //  }
    }
   // for(int i =0; i<10; i++)
/*    while(1==1)
    {
        //printf("Read from SPI: %02X%02X\n", data[0],data[1])
        // first we need to do some operation 
        b = (uint8_t)(127*sin(2*3.141*i/1000)+128); 
        i++; 
 
        a[1] =b;  
        if(!BCM2835_SPI0_CS_DONE){
        endTrans();                                  // finish the transmission now. 
        bcm2835_gpio_clr(25);
        bcm2835_spi_write4b(a,2);                   // This writes as many bytes as we want to the SPI (usuful for a DAC)
        bcm2835_gpio_set(25); 
        printf("high");

        // we send the data now. 
        }
        else{
        while(!BCM2835_SPI0_CS_DONE);             // we wait for the done bits to get ready. 
              endTrans();                               // finish the last transmission before the new one. 
           //   printf("low"); 
              bcm2835_gpio_clr(25);
              bcm2835_spi_write4b(a,2);                   // This writes as many bytes as we want to the SPI (usuful for a DAC)
              bcm2835_gpio_set(25);                 // we can finally write
      } 
        
    } */

    bcm2835_spi_end();
    bcm2835_close();
}