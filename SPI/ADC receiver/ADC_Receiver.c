#include <bcm2835.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

void setupSPI()
{
	bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // <-- we need to set the Most signficant bit first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // <--MODE0 = CPOL =0; CPHA =0; thus data is captured on rising edge and propogated on falling edege
    bcm2835_spi_setClockDivider(512); // <-- the clock divider
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // <-- we want this to be the chip that will be used (chip 0)
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // <-- I don't know why we have to set it low though. 
    
 }
  void closeSPI()
 {
 	bcm2835_spi_end();
    bcm2835_close();
 }
 void setup4MPCdac()
 {
    bcm2835_gpio_fsel(25, 1); // setting pin 25 as an output. 	<-- use only for the MCP DAC
 }



// Writes (and reads) an number of bytes to SPI
void spi_readnb( char *tbuf, char* rbuf, uint32_t len)
{
    volatile uint32_t* paddr = bcm2835_spi0 + BCM2835_SPI0_CS/4;
    volatile uint32_t* fifo = bcm2835_spi0 + BCM2835_SPI0_FIFO/4;
    

    // This is Polled transfer as per section 10.6.1
    // BUG ALERT: what happens if we get interupted in this section, and someone else
    // accesses a different peripheral? 

    // Clear TX and RX fifos
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_CLEAR, BCM2835_SPI0_CS_CLEAR);

    // Set TA = 1
    bcm2835_peri_set_bits(paddr, BCM2835_SPI0_CS_TA, BCM2835_SPI0_CS_TA);

    uint32_t i;
    for (i = 0; i < len; i++)
    {
    // Maybe wait for TXD
    while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_TXD))
        delayMicroseconds(10);
  //  printf("%s", "sending out the data\n");
    // Write to FIFO, no barrier
    bcm2835_peri_write_nb(fifo, tbuf[i]);

    // Wait for RXD
    while (!(bcm2835_peri_read(paddr) & BCM2835_SPI0_CS_RXD))
        delayMicroseconds(10);
  //  printf("%s", "reading in the data\n");
    // then read the data byte
    rbuf[i] = bcm2835_peri_read_nb(fifo);
    }
    // Wait for DONE to be set
    while (!(bcm2835_peri_read_nb(paddr) & BCM2835_SPI0_CS_DONE))
    delayMicroseconds(10);

    // Set TA = 0, and also set the barrier
    bcm2835_peri_set_bits(paddr, 0, BCM2835_SPI0_CS_TA);
}

void setRealtimeThread(){
   pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority=sched_get_priority_max(SCHED_FIFO);
    int success = pthread_setschedparam(this_thread,SCHED_FIFO,&params);
    if(success != 1){
      printf("Set real time thread priority: Success\n");
    }else{
      printf("Set real time thread priority: Fail\n");
    }
    int policy = 1;
    int ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        printf("ERROR: Couldn't retrieve real-time scheduling paramers\n");
    }
 
    // Check the correct policy was applied
    if(policy != SCHED_FIFO) {
        printf("WARNING: Scheduling is NOT SCHED_FIFO\nWARNING: Policy = %d",policy);
    } else {
        printf("SCHED_FIFO: Ok\n");
    }
    // Print thread scheduling priority
    printf("Thread priority: %d \n", params.sched_priority);
}

char * bin2character(int *bin)
{
    char *string; 
    return string; 
}


int main()
{
  //  setRealtimeThread(); 
	  if (!bcm2835_init()) // if that shizz fails... 
	    return -1;

    setRealtimeThread();  // setting the thread priority

	setupSPI();            // setting up the SPI 

    const int len = 2; 

    uint8_t a[2] ={0x00,0x00};  // we only want to send zeros to the ADC -> since we aren't actually going to be 
                                            // sending anything to the ADC, there's no pin for that, so this can all be set high, 
                                            // but we might as well just set it all low and save power. 
    uint8_t b[2] ={0x00,0x00}; 
    uint8_t test_data; 
    uint16_t data_A = 0;    // Data from ADC A
    uint16_t data_B = 0;    // Data from ADC B
    uint16_t temp =0;          // temp value for the data handling procedure 

    float DataA = 0.0;         // the raw voltage 'seen' by the ADC at channel A
    float DataB = 0.0;         // the raw voltage 'seen' by the ADC at channel B

    float DataA_previous=0.0; 
    float DataB_previous=0.0; 

    float DC_Baseline_A =0.0; 
    float DC_Baseline_B =0.0; 

    float DeltaA =0.0; 
    float DeltaB =0.0;
    
    const float volts_per_bit = 5.0/4096.0; // this is the data conversion that we will be using.
    bcm2835_gpio_fsel(25, 1);  // setting pin 25 as an output. 
    long i =0;

    int isStable = 0; 

    int StartedReceiving =0;

    int received[89] = {0}; 

    int byte=0; 
    int counter =0; 
    int counter2 = 0;
    int counter3 = 0;

    float thresholdA = 0.07; 
    float thresholdB= 0.07; 
    float thresholdC= 0.1;

    float bitA = 0.0; 
    float bitB = 0.0; 

    int digitalMode =0; 

    int temp_counter =0;

    float temp_A[396] ={0};
    float temp_B[396] ={0}; 

    int data_length = 396; 

    long sleepyTime = 83; 

   while (1)
    {
        
      //  printf("%s", "begining the voltage reading process\n");
      //  bcm2835_gpio_clr(25);   // pin 25 will be our chip selector -> setting to zero will turn on the whole process
        // a is a 4 byte (32 bit) value that will be sent and received from the SPI module 
        // we are sending 32 bits since we need to hold the control pin low for two full data transfers 
        // allowing for both values from the ADC channels to be read simulatiously. This is essential since we need to receive 
        // both ADC values at the same time instance to prevent phase shifts in the data (this is especially important for digital tranmission)

       // printf("starting the conversion\n");
        bcm2835_spi_chipSelect(0);
        spi_readnb(a,a, len); 
       // usleep(1000); 
        bcm2835_spi_chipSelect(1);
        spi_readnb(b,b, len); 

        // A channel
        test_data = a[0]<<3; 
        test_data = test_data>>3; 
        temp = (uint16_t)test_data; 
        data_A = temp<<8; 
        test_data = a[1]; 
        temp = (uint16_t)test_data; 
        data_A = (data_A+test_data)>>1; 
        // B Channel 
        test_data = b[0]<<3; 
        test_data = test_data>>3; 
        temp = (uint16_t)test_data; 
        data_B = temp<<8; 
        test_data = a[1]; 
        temp = (uint16_t)test_data; 
        data_B = (data_B+test_data)>>1; 

        DataA = (float)(data_A)*volts_per_bit; // converting binary to a voltage value (that's actually relevant ) for channel A

        DataB = (float)(data_B)*volts_per_bit; // converting binary to a voltage value (that's actually relevant ) for channel A

      //  usleep(250); 

    //    temp_B[temp_counter] = DataB; 
     //   temp_A[temp_counter] = DataA; 
     //   temp_counter = temp_counter +1; 

//        printf("%s%f%s", "DataA: ",DataA, "\n");

      // printf("%s%f%s", "DataA: ",DataA, "\n");
      // printf("%s%f%s", "DataB: ",DataB, "\n");





      // printf("%s%d%s","stable? ",isStable,"\n"); 
        if(isStable==0)
        {
            DC_Baseline_A = (DataA*0.1+DC_Baseline_A*0.9);  // low pass filter
            DC_Baseline_B = (DataB*0.1+DC_Baseline_B*0.9); 
           // printf("%s%f%s", "voltage reading baseline_A: ", DC_Baseline_A ,"\n");
           // printf("%s%f%s", "voltage reading Baseline_B: ", DC_Baseline_B, "\n");
          //  printf("%s%f%s","Data: ",DataA,"\n");
            counter++; 
            //usleep(sleepyTime); 
        }
    
       
         if(counter>=2000&&isStable==0)
            {
                isStable = 1; 
                counter==0; 
                printf("%s%f%s","DC_Baseline A", DC_Baseline_B,"\n");
                 printf("%s%f%s","DC_Baseline B", DC_Baseline_A, "\n");
               // printf("%s%f","\nSystem is isStable with a DC_Baseline of: ",DC_Baseline_B); 
            }
        
       
    if (digitalMode==0&&isStable==1){
       DeltaA = DataA - DC_Baseline_A;
       DeltaB = DataB - DC_Baseline_B;

       //printf("%s%f", "\nvoltage reading deltaA: ", DeltaA );
       //printf("%s%f", "\nvoltage reading deltaB: ", DeltaB );

        if((DeltaA>thresholdA||DeltaA<-1*thresholdA|| DeltaB>thresholdB||DeltaB<-1*thresholdB)&&isStable){
           // counter2++;
            //int mod = counter2%100; 
            digitalMode=1; 
            DeltaA = DataA;
            DeltaB = DataB; 
           // if((counter2-mod*100)==0){
             //printf("%s%f", "\nvoltage reading deltaA: ", DeltaA );
            //}
            //usleep(1000); 
        } 
        DeltaA = DataA;
        DeltaB = DataB;
      //  usleep(sleepyTime); 
    }
    else if(digitalMode==1&&isStable==1)
    {
        bitA = DataA-DeltaA; 
        bitB = DataB-DeltaB;
       // printf("%s%f%s","DataA value: ",bitA,"\n"); 
        //printf("%s%f%s","DataB value: ",bitB,"\n"); 
    /*    if (counter2 >0)
        {
            if(bitA>thresholdC||bitA<-1*thresholdC||bitB>thresholdC||bitB<-1*thresholdC)
            {

                counter3++;

                received[counter2] = 1;
             //    printf("%s%d%s","The number of bits received: ",counter3," High\n"); 
            }
            else 
            {
                counter3++;
                received[counter2] = 0; 
               // printf("%s%d%s","The number of bits received: ",counter3," low\n"); 
            }

        }
    */


        temp_A[counter3] = DeltaA; 
        temp_B[counter3] = DeltaB; 

        counter3++; 
      /*  if(counter2==4095)
        {
            printf("broken from the receiver\n"); 
            counter2 =0; 
            digitalMode=0; // leave digital mode
            usleep(1000000); 
        } */
        DeltaA = DataA; 
        DeltaB = DataB; 
       // usleep(sleepyTime*9);

        if(counter3== data_length)
        {
            break; 
        }
    }

    /*   if(DeltaA>threshold||DeltaA<-threshold||DeltaB>threshold||DeltaB<-threshold)
       {
            if()
       } */


    /*   if(isStable&&StartedReceiving)
       {

            DeltaA = DeltaA - DataA; 
            DeltaB = DeltaB - DataB; 

            if(DeltaA+DeltaB<0.01)
            {

            }
       }*/

    //   DeltaA = DataA; 
   //    DeltaB = DataB; 


 //       printf("%s%f%s", "voltage reading from DC A: ", DC_Baseline_A , "\n");
  //      printf("%s%f%s", "voltage reading from DC B: ", DC_Baseline_B , "\n");


     /*   printf("%s%f%s", "voltage reading from A: ", DataA-DataA_previous , "\n");  // lets print out the voltage measured from the ADC.
        printf("%s%f%s", "voltage reading from A Voltage: ", DataA , "\n");  // lets print out the voltage measured from the ADC.
        printf("%s%f%s", "voltage reading from B: ", DataB-DataB_previous, "\n");  // From both data ports.
        printf("%s%f%s", "voltage reading from B Voltage: ", DataB, "\n");  // From both data ports.  */ 
        //DataA_previous = DataA; 
       // DataB_previous = DataB; 
        // flushing out a for the next data transfer. -> set low for the luxury of not wasting power. 
        a[0] = 0x00; 
        a[1] = 0x00;  
        b[0] = 0x00; 
        b[1] = 0x00;
        usleep(sleepyTime); 
    
        // setting to 0.5 second of sleep. 
      //  usleep(1000);         // just setting to 100 usleep for this -> allows for the the RPi to sleep every so often, and lowers the power. 
    }

    printf("\nA line:");
    for (temp_counter = 0; temp_counter<data_length; temp_counter++)
    {
        printf("%f%s", temp_A[temp_counter]," ");
    }

    printf("\nB line:");
    for (temp_counter = 0; temp_counter<data_length; temp_counter++)
    {
        printf("%f%s", temp_B[temp_counter]," ");
    }
    printf("\n");



    // time for some binary decoding: 

    int binaryData1[9] ={0};
    int sample_counter= 0;
    float sampled_data = 0.0; 
    float prev_sampled = 0.0; 

    int offset =4;
    float thresh = 0.1; 
    float delta =0.0;

    for (temp_counter =0; temp_counter<9; temp_counter++)
    {
        sampled_data=0; 
        for (sample_counter =0; sample_counter<offset; sample_counter++)
        {
            sampled_data = temp_A[sample_counter+(temp_counter)*offset]+sampled_data; 
        }

        sampled_data = sampled_data/offset; 
        

       // sampled_data = sampled_data- DC_Baseline_A;
        
        delta = sampled_data - prev_sampled;
        printf("%f%s", delta,"\n");

        
        if((delta>thresh||delta<-1*thresh)&&temp_counter>0)
        {
            binaryData1[temp_counter] = 0; 
        }
        else if(temp_counter ==0)
        {
            binaryData1[0] =0; 
        }
        else
        {
            binaryData1[temp_counter] = 1; 
        }

        prev_sampled = sampled_data;
    }
    int binaryData2[9] ={0};
    sample_counter= 0;
    sampled_data = 0.0; 
    prev_sampled = 0.0; 
    delta =0.0;
    temp_counter =0;

    for (temp_counter =0; temp_counter<9; temp_counter++)
    {
        sampled_data=0;
        for (sample_counter =0; sample_counter<offset; sample_counter++)
        {
            sampled_data = temp_A[sample_counter+(temp_counter)*offset]+sampled_data; 
        }

        sampled_data = sampled_data/offset; 


        delta = sampled_data - prev_sampled;
        
        

        if((delta>0.1||delta<-0.1)&&temp_counter>0)
        {
            binaryData2[temp_counter] = 0; 
        }
        else if(temp_counter ==0)
        {
            binaryData2[0] =0; 
        }
        else
        {
            binaryData2[temp_counter] = 1; 
        }
        prev_sampled = sampled_data;
    }


    int binaryData3[9] ={0};
    sample_counter= 0;
    sampled_data = 0.0; 
    prev_sampled = 0.0; 
    delta =0.0;
    temp_counter =0;

    for (temp_counter =0; temp_counter<9; temp_counter++)
    {
        sampled_data=0;
        for (sample_counter =0; sample_counter<offset; sample_counter++)
        {
            sampled_data = temp_A[sample_counter+(temp_counter)*offset]+temp_B[sample_counter+(temp_counter)*offset]+sampled_data; 
        }

        sampled_data = sampled_data/offset; 


        delta = sampled_data - prev_sampled;
        
        

        if((delta>0.1||delta<-0.1)&&temp_counter>0)
        {
            binaryData3[temp_counter] = 0; 
        }
        else if(temp_counter ==0)
        {
            binaryData3[0] =0; 
        }
        else
        {
            binaryData3[temp_counter] = 1; 
        }
        prev_sampled = sampled_data;
    }


    printf("\nA line:");
    for (temp_counter = 0; temp_counter<9; temp_counter++)
    {
        printf("%d%s", binaryData1[temp_counter]," ");
    }

    printf("\nB line:");
    for (temp_counter = 0; temp_counter<9;  temp_counter++)
    {
        printf("%d%s", binaryData2[temp_counter]," ");
    }


    printf("\nAddition line:");
    for (temp_counter = 0; temp_counter<9;  temp_counter++)
    {
        printf("%d%s", binaryData3[temp_counter]," ");
    }

    printf("\n");




    closeSPI();

}
